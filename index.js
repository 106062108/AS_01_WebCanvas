var myCanvas;
var ctx;

// default values
var default_canvas_width = 600;
var default_canvas_height = 600;
var default_canvas_border = "0px solid black;";

var mouseClicked;
var lastButtonClicked;

var prevX, prevY;

// pencil properties
var pencil_color;
var pencil_size;

// eraser properties
var eraser_color;
var eraser_size;

// filled
var circleFilled;
var rectangleFilled;
var triangleFilled;

// gradient
var gradient_counter = {r: 0, r_dir:0, g: 0, g_dir:0, b: 0, b_dir:0};
var gradient_velocity = 1; // recommend range : 1 ~ 255
var gradient_success_rate = 0.9; // recommend range : 0.0 ~ 1.0

// image path
var stroke_circle_image_path = "./image/stroke_circle_icon.png";
var filled_circle_image_path = "./image/filled_circle_icon.png";
var stroke_rectangle_image_path = "./image/stroke_rectangle_icon.png";
var filled_rectangle_image_path = "./image/filled_rectangle_icon.png";
var stroke_triangle_image_path = "./image/stroke_triangle_icon.png";
var filled_triangle_image_path = "./image/filled_triangle_icon.png";

// image array
var imageArray;
var imageArrayPtr;

// image snapshot
var imageSnapshot;

// button shadow effect
var defaultShadowEffect = "0px 0px 0px 0px";
var clickedShadowEffect = "white 0px 0px 3px 3px";
var hoverShadowEffect = "black 3px 3px 0px 0px";
var activeShadowEffect = "grey 0px 0px 3px 3px";

function Initialization() {
    console.log("Initialization()");
    
    var button_inner = document.getElementsByClassName("button-inner");
    for(var i = 0; i < button_inner.length; i++) {
        button_inner[i].addEventListener("click", function() {updateLastButtonClicked(event)});
    }

    // button shadow effect
    var button = document.getElementsByClassName("button");
    for(var i = 0; i < button.length; i++) {
        button[i].addEventListener("mouseover", function(){
            if(this.style.boxShadow != clickedShadowEffect) {
                this.style.boxShadow = hoverShadowEffect;
            }
        });
        button[i].addEventListener("mouseleave", function(){
            if(this.style.boxShadow != clickedShadowEffect) {
                this.style.boxShadow = defaultShadowEffect;
            }
        });
        button[i].addEventListener("mousedown", function() {
            this.style.boxShadow = activeShadowEffect;
        });
        button[i].addEventListener("mouseup", function() {
            this.style.boxShadow = defaultShadowEffect;
        });
    }

    text_submit_button = document.getElementById("text-submit");
    text_submit_button.addEventListener("click", function() {updateLastButtonClicked(event)});

    myCanvas = document.getElementById("myCanvas");
    ctx = myCanvas.getContext("2d");

    myCanvas.addEventListener("mousedown", function() {mouseDown(event)});
    myCanvas.addEventListener("mousemove", function() {mouseMove(event)});
    myCanvas.addEventListener("mouseup", function() {mouseUp(event)});
    myCanvas.addEventListener("mouseleave", function() {mouseLeave(event)});

    // upload
    document.getElementById("upload-btn-inner").addEventListener("click", function() {UploadImage()});

    // download
    document.getElementById("download-btn-inner").addEventListener("click", function() {DownloadImage()});
    
    // cursor
    document.getElementById("cursor-btn-selector").addEventListener("change", function() {CursorChange()});

    // reset
    document.getElementById("reset-btn-inner").addEventListener("click", function() {ResetCanvas()});

    // undo
    document.getElementById("undo-btn-inner").addEventListener("click", function() {ImageUndo()});

    // redo
    document.getElementById("redo-btn-inner").addEventListener("click", function() {ImageRedo()});

    // default values
    myCanvas.width = default_canvas_width;
    myCanvas.height = default_canvas_height;
    myCanvas.style.border = default_canvas_border;
    
    mouseClicked = false;
    lastButtonClicked = document.getElementById("pencil-btn-inner");

    prevX = 0;
    prevY = 0;
    
    // pencil properties
    pencil_color = document.getElementById("pencil-color").value;
    pencil_size = document.getElementById("pencil-size").value;

    // eraser properties
    eraser_color = document.getElementById("eraser-color").value;
    eraser_size = document.getElementById("eraser-size").value;

    // filled
    circleFilled = false;
    rectangleFilled = false;
    triangleFilled = false;

    // image array
    var imageArray_initial_element = {
        imageUrl: myCanvas.toDataURL(),
        canvas_width: default_canvas_width,
        canvas_height: default_canvas_height
    };
    imageArray = new Array();
    imageArray.push(imageArray_initial_element);
    imageArrayPtr = 0;

    // image snapshot
    imageSnapshot = null;

    // button shadow effect
    document.getElementById("pencil-btn").style.boxShadow = clickedShadowEffect;
}

function updateLastButtonClicked(event) {
    var newButtonClicked = document.elementFromPoint(event.clientX, event.clientY);
    switch (newButtonClicked) {
        case document.getElementById("upload-btn-inner"):
            break;
        case document.getElementById("download-btn-inner"):
            break;
        case document.getElementById("reset-btn-inner"):
            break;
        case document.getElementById("undo-btn-inner"):
            break;
        case document.getElementById("redo-btn-inner"):
            break; 
        default:
            lastButtonClicked.parentElement.style.boxShadow = defaultShadowEffect;
            newButtonClicked.parentElement.style.boxShadow = clickedShadowEffect;
            lastButtonClicked = newButtonClicked;
            break;
    }
    console.log("newButtonClicked : " + newButtonClicked.id);
    console.log("lastButtonClicked : " + lastButtonClicked.id);

    switch (newButtonClicked) {
        case(document.getElementById("circle-btn-inner")):
            if(circleFilled) {
                circleFilled = false;
                document.getElementById("circle-btn-inner").src = stroke_circle_image_path;
            }
            else {
                circleFilled = true;
                document.getElementById("circle-btn-inner").src = filled_circle_image_path;
            }
            break;
        case(document.getElementById("rectangle-btn-inner")):
            if(rectangleFilled) {
                rectangleFilled = false;
                document.getElementById("rectangle-btn-inner").src = stroke_rectangle_image_path;
            }
            else {
                rectangleFilled = true;
                document.getElementById("rectangle-btn-inner").src = filled_rectangle_image_path
            }
            break;
        case(document.getElementById("triangle-btn-inner")):
            if(triangleFilled) {
                triangleFilled = false;
                document.getElementById("triangle-btn-inner").src = stroke_triangle_image_path
            }
            else {
                triangleFilled = true;
                document.getElementById("triangle-btn-inner").src = filled_triangle_image_path;
            }
            break;
    }
}

function getMousePos(event) {
    var rect = myCanvas.getBoundingClientRect();
    return {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
}

function twoPointsDistance(x1, y1, x2, y2) {
    return Math.sqrt( Math.pow(Math.abs(x1-x2), 2) + Math.pow(Math.abs(y1-y2), 2));
}

function mouseDown(event) {
    mouseClicked = true;

    var pos = getMousePos(event);
    Draw(pos.x, pos.y, false);
}

function mouseMove(event) {
    var pos = getMousePos(event);
    if(mouseClicked) {
        Draw(pos.x, pos.y, true);
    }
}

function mouseUp(event) {
    mouseClicked = false;

    var pos = getMousePos(event);
    Draw(pos.x, pos.y, false);

    ImagePush();
}

function mouseLeave(event) {
    mouseClicked = false;
}

function takeImageSnapshot() {
    imageSnapshot = ctx.getImageData(0, 0, myCanvas.width, myCanvas.height);
}

function restoreImageSnapshot() {
    ctx.putImageData(imageSnapshot, 0, 0);
}

function Draw(x, y, isDown) {
    InkConfirm();
    switch (lastButtonClicked) {
        case document.getElementById("pencil-btn-inner"):
            PencilDraw(x, y, isDown);
            break;
        case document.getElementById("eraser-btn-inner"):
            EraserDraw(x, y, isDown);
            break;
        case document.getElementById("line-btn-inner"):
            LineDraw(x, y, isDown);
            break;
        case document.getElementById("circle-btn-inner"):
            CircleDraw(x, y, isDown);
            break;
        case document.getElementById("rectangle-btn-inner"):
            RectangleDraw(x, y, isDown);
            break;
        case document.getElementById("triangle-btn-inner"):
            TriangleDraw(x, y, isDown);
            break;
        case document.getElementById("text-submit"):
            TextDraw(x, y, isDown);
            break;
        case document.getElementById("gradient-btn-inner"):
            GradientDraw(x, y, isDown);
            break;
        default:
            return;
    }
}

function PencilDraw(x, y, isDown) {
    ctx.strokeStyle = pencil_color;
    ctx.lineWidth = pencil_size;
    
    if(isDown) {
        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(x, y);
        ctx.stroke();
    }
    prevX = x;
    prevY = y;
}

function EraserDraw(x, y, isDown) {
    ctx.strokeStyle = eraser_color;
    ctx.lineWidth = eraser_size;
    
    if (isDown) {
        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(x, y);
        ctx.stroke();
    }
    prevX = x;
    prevY = y;
}

function LineDraw(x, y, isDown) {
    ctx.strokeStyle = pencil_color;
    ctx.lineWidth = pencil_size;

    if(isDown) {
        restoreImageSnapshot();

        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(x, y);
        ctx.stroke();
    }
    else {
        if(mouseClicked) {
            prevX = x;
            prevY = y;

            takeImageSnapshot();
        }
        else {
            restoreImageSnapshot();

            ctx.beginPath();
            ctx.moveTo(prevX, prevY);
            ctx.lineTo(x, y);
            ctx.stroke();
        }
    }
}

function InkConfirm() {
    pencil_color = document.getElementById("pencil-color").value;
    pencil_size = document.getElementById("pencil-size").value;
    
    eraser_color = document.getElementById("eraser-color").value;
    eraser_size = document.getElementById("eraser-size").value;
}

function CircleDraw(x, y, isDown) {
    ctx.strokeStyle = pencil_color;
    ctx.lineWidth = pencil_size;
    ctx.fillStyle = pencil_color;

    if(isDown) {
        restoreImageSnapshot();
            
        ctx.beginPath();
        ctx.arc(prevX, prevY, twoPointsDistance(prevX, prevY, x, y), 0, 2*Math.PI);
        if(circleFilled) {
            ctx.fill();
        }
        ctx.stroke();
    }
    else {
        if(mouseClicked) {
            takeImageSnapshot();
            
            prevX = x;
            prevY = y;
        }
        else {
            restoreImageSnapshot();
            
            ctx.beginPath();
            ctx.arc(prevX, prevY, twoPointsDistance(prevX, prevY, x, y), 0, 2*Math.PI);
            if(circleFilled) {
                ctx.fill();
            }
            ctx.stroke();
        }
    }
}

function RectangleDraw(x, y, isDown) {
    ctx.strokeStyle = pencil_color;
    ctx.lineWidth = pencil_size;
    ctx.fillStyle = pencil_color;

    if(isDown) {
        restoreImageSnapshot();

        ctx.beginPath();
        ctx.rect(Math.min(prevX, x), Math.min(prevY, y), Math.abs(prevX-x), Math.abs(prevY-y));
        if(rectangleFilled) {
            ctx.fill();
        }
        ctx.stroke();
    }
    else {
        if(mouseClicked) {
            prevX = x;
            prevY = y;

            takeImageSnapshot();
        }
        else {
            restoreImageSnapshot();
            
            ctx.beginPath();
            ctx.rect(Math.min(prevX, x), Math.min(prevY, y), Math.abs(prevX-x), Math.abs(prevY-y));
            if(rectangleFilled) {
                ctx.fill();
            }
            ctx.stroke();
        }
    }
}

function TriangleDraw(x, y, isDown) {  
    ctx.strokeStyle = pencil_color;
    ctx.lineWidth = pencil_size;
    ctx.fillStyle = pencil_color;

    if(isDown) {
        restoreImageSnapshot();
            
        ctx.beginPath();
        triangleDrawImpl(prevX, prevY, x, y);
        if(triangleFilled) {
            ctx.fill();
        }
        ctx.stroke();
    }
    else {
        if(mouseClicked) {
            prevX = x;
            prevY = y;

            takeImageSnapshot();
        }
        else {
            restoreImageSnapshot();
            
            ctx.beginPath();
            triangleDrawImpl(prevX, prevY, x, y);
            if(triangleFilled) {
                ctx.fill();
            }
            ctx.stroke();
        }
    }
}

function triangleDrawImpl(centerX, centerY, vertexX1, vertexY1) {
    var distance = twoPointsDistance(centerX, centerY, vertexX1, vertexY1);
    
    var vector1 = {
        x: vertexX1 - centerX,
        y: vertexY1 - centerY
    };
    var vector2 = {
        x: vector1.x * Math.cos(2*Math.PI/3) - vector1.y * Math.sin(2*Math.PI/3),
        y: vector1.x * Math.sin(2*Math.PI/3) + vector1.y * Math.cos(2*Math.PI/3)
    };
    var vector3 = {
        x: vector1.x * Math.cos(-2*Math.PI/3) - vector1.y * Math.sin(-2*Math.PI/3),
        y: vector1.x * Math.sin(-2*Math.PI/3) + vector1.y * Math.cos(-2*Math.PI/3)
    };

    var vertex1 = {
        x: vertexX1,
        y: vertexY1
    };
    var vertex2 = {
        x: centerX + vector2.x,
        y: centerY + vector2.y
    }
    var vertex3 = {
        x: centerX + vector3.x,
        y: centerY + vector3.y
    }

    ctx.moveTo(vertex1.x, vertex1.y);
    ctx.lineTo(vertex2.x, vertex2.y);
    ctx.lineTo(vertex3.x, vertex3.y);
    ctx.closePath();
}

function TextDraw(x, y, isDown) {
    var text_content = document.getElementById("text-content");
    var text_content_value = text_content.value;
    var text_font = document.getElementById("text-font");
    var text_font_value = text_font.options[text_font.selectedIndex].value;
    var text_filled = document.getElementById("text-filled");
    var text_filled_value = text_filled.checked;
    var text_font_size_value = Math.abs(prevY-y);
    var text_font_input = text_font_size_value + "px " + text_font_value;
    
    if(isDown) {
        restoreImageSnapshot();

        ctx.fillStyle = pencil_color;
        ctx.font = text_font_input;
        if(text_filled_value == true) {
            ctx.fillText(text_content_value, Math.min(prevX, x), Math.max(prevY, y), Math.abs(prevX-x));
        }
        else {
            ctx.strokeText(text_content_value, Math.min(prevX, x), Math.max(prevY, y), Math.abs(prevX-x));
        }
    }
    else {
        if(mouseClicked) {
            prevX = x;
            prevY = y;

            takeImageSnapshot();
        }
        else {          
            restoreImageSnapshot();
            
            ctx.fillStyle = pencil_color;
            ctx.font = text_font_input;
            if(text_filled_value == true) {
                ctx.fillText(text_content_value, Math.min(prevX, x), Math.max(prevY, y), Math.abs(prevX-x));
            }
            else {
                ctx.strokeText(text_content_value, Math.min(prevX, x), Math.max(prevY, y), Math.abs(prevX-x));
            }
        }
    }
}

function GradientDraw(x, y, isDown) {
    if(isDown) {
        ctx.beginPath();
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(x, y);
        ctx.stroke();

        ctx.strokeStyle = "rgb(" + gradient_counter.r + ", " + gradient_counter.g + ", " + gradient_counter.b + ")";
        ctx.lineWidth = pencil_size;
        
        // gradient_counter.r
        if(gradient_counter.r <= 0) {
            gradient_counter.r += gradient_velocity * ((Math.random() < gradient_success_rate) ? 1 : 0);
            gradient_counter.r_dir = 0;
        }
        else if(gradient_counter.r > 0 && gradient_counter.r < 255) {
            if(gradient_counter.r_dir == 0) { // increment
                gradient_counter.r += gradient_velocity * ((Math.random() < gradient_success_rate) ? 1 : 0);
            }
            else if(gradient_counter.r_dir == 1) { // decrement
                gradient_counter.r -= gradient_velocity * ((Math.random() < gradient_success_rate) ? 1 : 0);
            }
        }
        else if(gradient_counter.r >= 255) {
            gradient_counter.r -= gradient_velocity * ((Math.random() < gradient_success_rate) ? 1 : 0);
            gradient_counter.r_dir = 1;
        }

        // gradient_counter.g
        if(gradient_counter.g <= 0) {
            gradient_counter.g += gradient_velocity * ((Math.random() < gradient_success_rate) ? 1 : 0);
            gradient_counter.g_dir = 0;
        }
        else if(gradient_counter.g > 0 && gradient_counter.g < 255) {
            if(gradient_counter.g_dir == 0) { // increment
                gradient_counter.g += gradient_velocity * ((Math.random() < gradient_success_rate) ? 1 : 0);
            }
            else if(gradient_counter.g_dir == 1) { // decrement
                gradient_counter.g -= gradient_velocity * ((Math.random() < gradient_success_rate) ? 1 : 0);
            }
        }
        else if(gradient_counter.g >= 255) {
            gradient_counter.g -= gradient_velocity * ((Math.random() < gradient_success_rate) ? 1 : 0);
            gradient_counter.g_dir = 1;
        }

        // gradient_counter.b
        if(gradient_counter.b <= 0) {
            gradient_counter.b += gradient_velocity * Math.floor(Math.random()*2);
            gradient_counter.b_dir = 0;
        }
        else if(gradient_counter.b > 0 && gradient_counter.b < 255) {
            if(gradient_counter.b_dir == 0) { // increment
                gradient_counter.b += gradient_velocity * Math.floor(Math.random()*2);
            }
            else if(gradient_counter.b_dir == 1) { // decrement
                gradient_counter.b -= gradient_velocity * Math.floor(Math.random()*2);
            }
        }
        else if(gradient_counter.b >= 255) {
            gradient_counter.b -= gradient_velocity * Math.floor(Math.random()*2);
            gradient_counter.b_dir = 1;
        }
    }
    else {
        gradient_counter = {r: Math.floor(Math.random()*256), r_dir: Math.floor(Math.random()*2), 
                            g: Math.floor(Math.random()*256), g_dir: Math.floor(Math.random()*2),
                            b: Math.floor(Math.random()*256), b_dir: Math.floor(Math.random()*2)};

        ctx.strokeStyle = "rgb(" + gradient_counter.r + ", " + gradient_counter.g + ", " + gradient_counter.b + ")";
        ctx.lineWidth = pencil_size;
    }
    prevX = x;
    prevY = y;
}

function UploadImage() {
    var image_upload_link = document.createElement("input");

    image_upload_link.type = "file";
    image_upload_link.accept = "image/*";
    
    image_upload_link.click();
    image_upload_link.addEventListener("change", function() {
        var newImage = new Image();

        newImage.src = URL.createObjectURL(image_upload_link.files[0]);
        newImage.addEventListener("load", function() {
            myCanvas.width = newImage.width;
            myCanvas.height = newImage.height;
            ctx.drawImage(newImage, 0, 0);
            ImagePush();
        });
    });
}

function DownloadImage() {
    var image_download_link = document.createElement("a");
    var date = new Date();
    var date_time = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate() + " " + date.getHours() + "-" + date.getMinutes() + "-" + date.getSeconds(); 
    var file_name = "My-Painter " + date_time + ".png";

    image_download_link.href = myCanvas.toDataURL("image/jpeg");
    image_download_link.download = file_name;
    image_download_link.click();
}

function CursorChange() {
    var cursor_selector = document.getElementById("cursor-btn-selector");
    var cursor_selector_value = cursor_selector.options[cursor_selector.selectedIndex].value;

    var cursor_image = document.getElementById("cursor-image");
    var cursor_image_url = "./cursor/cursor_" + cursor_selector_value + ".cur";

    myCanvas.style.cursor = 'url("' + cursor_image_url + '"), default';
    cursor_image.src = cursor_image_url;
}

function ResetCanvas() {
    ctx.clearRect(0, 0, myCanvas.width, myCanvas.height);

    // default values
    myCanvas.width = default_canvas_width;
    myCanvas.height = default_canvas_height;
    myCanvas.style.border = default_canvas_border;

    mouseClicked = false;
    
    prevX = 0;
    prevY = 0;
    
    // pencil properties
    pencil_color = document.getElementById("pencil-color").value;
    pencil_size = document.getElementById("pencil-size").value;

    // eraser properties
    eraser_color = document.getElementById("eraser-color").value;
    eraser_size = document.getElementById("eraser-size").value;

    // filled
    circleFilled = false;
    rectangleFilled = false;
    triangleFilled = false;

    // image array
    imageArray = [];
    imageArray.push(myCanvas.toDataURL());
    imageArray.length = 1;
    imageArrayPtr = 0;

    // image snapshot
    imageSnapshot = null;
}

function ImagePush() {
    var imageArray_new_element = {
        imageUrl: myCanvas.toDataURL(),
        canvas_width: myCanvas.width,
        canvas_height: myCanvas.height
    };
    
    imageArrayPtr++;
    if(imageArrayPtr < imageArray.length) {
        imageArray.length = imageArrayPtr;
    }
    imageArray.push(imageArray_new_element);
}

function ImageUndo() {
    if(imageArrayPtr > 0) {
        imageArrayPtr--;

        var newImage = new Image();
        newImage.src = imageArray[imageArrayPtr].imageUrl;
        newImage.addEventListener("load", function() {
            ctx.clearRect(0, 0, myCanvas.width, myCanvas.height);
            ctx.drawImage(newImage, 0, 0);
        });

        myCanvas.width = imageArray[imageArrayPtr].canvas_width;
        myCanvas.height = imageArray[imageArrayPtr].canvas_height;
    }
}

function ImageRedo() {
    if(imageArrayPtr < imageArray.length-1) {
        imageArrayPtr++;

        var newImage = new Image();
        newImage.src = imageArray[imageArrayPtr].imageUrl;
        newImage.addEventListener("load", function() {
            ctx.clearRect(0, 0, myCanvas.width, myCanvas.height);
            ctx.drawImage(newImage, 0, 0);
        });

        myCanvas.width = imageArray[imageArrayPtr].canvas_width;
        myCanvas.height = imageArray[imageArrayPtr].canvas_height;
    }
}