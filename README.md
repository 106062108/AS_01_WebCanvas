# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

### Authored by 106062108 劉岳濬, CS Department NTHU, Taiwan
### Lastly update at 2019.04.04

### There're totally 15 functionalities in this project. All of them will be introduced respectively as follows.

### Schematic diagram of the website :
    |  Canvas Area  | Buttons Area |
    |:-------------:|:------------:|
    |               | 01 | 02 | 03 |
    |               | 04 | 05 | 06 |
    | Painting Area | 07 | 08 | 09 |
    |               | 10 | 11 | 12 |
    |               | 13 | 14 | 15 |

### 01. Cursor Menu
*       User can change cursor icon in canvas area using Cursor Menu.
*       Different cursor icon can be chosen in the dropdown list.
*       Current used cursor icon will be displayed under the dropdown list.

### 02. Ink Menu
*     User can change pencil's and eraser's ink using Ink Menu.
*     For both pencil and eraser, user can change their color and brush size.
*     Click color picker to change color, and click number selector to change brush size (from 1 ~ 5).

### 03. Text Menu
*       User can draw filled of stroke text in canvas area using Text Menu.
*       Following are steps of drawing text in canvas area:
          a. Select text font in the dropdown list.
          b. Type the text that you want to draw.
          c. Click the "Text Filled" for a filled text, and unclick it for a stroke text.
          d. Click "Draw" button, and then drag mouse in canvas area to resize a satisfyed size of text.
          e. Drop mouse to finish drawing of text.

### 04. Pencil
*       User can draw smooth curved line in canvas area using Pencil.
*       Rember to check pencil's color and brush size before you draw.

### 05. Eraser
*       User can eraser smooth curved line in canvas area using Eraser.
*       Rember to check eraser's color and brush size before you eraser.

### 06. Line
*       User can draw straight line in canvas area using Line.
*       Rember to check pencil's color and brush size before you draw.

### 07. Circle
*       User can draw filled of stroke circle in canvas area using Circle.
*       Click first to enable Circle service; then, click more once to change between filled and stroke mode.
*       Rember to check pencil's color and brush size before you draw.

### 08. Rectangle
*       User can draw filled of stroke rectangle in canvas area using Rectangle.
*       Click first to enable Rectangle service; then, click more once to change between filled and stroke mode.
*       Rember to check pencil's color and brush size before you draw.

### 09. Triangle
*       User can draw filled of stroke regular triangle in canvas area using Triangle.
*       Click first to enable Triangle service; then, click more once to change between filled and stroke mode.
*       While Draging your mouse in canvas area, the point that your mouse points on is one of verticles of the regular triangle.
*       Rember to check pencil's color and brush size before you draw.

### 10. Upload
*       User can upload image in canvas area using Upload.
*       Following are steps of uploading image in canvas area:
          a. Click upload button to open file selector.
          b. Select the image that you want to upload. Files which are not stored as image types will not be allowed to upload.
          c. Click the confirm button to choose the image.
          d. Then, the image will be automatically drawn in canvas area from the top-left.
          e. If the uploaded image's size is larger than canvas', it will automatically resized to fit the uploaded image's size.

### 11. Download
*       User can download drawing in canvas area using Download.
*       Click download button, and then drawing in canvas area will be automatically download to your host.

### 12. Gradient
*       User can draw smooth curved gradient line in canvas area using Gradient.
*       Following are steps of drawing smooth curved gradient line in canvas area:
          a. Random color will be automatically selected at the moment that user mousedown in canvas area.
          b. Then, when user move mouse in canvas area, color of pencil will automatically changed gradiently.
          c. The above automatically color-changed will complete at the moment that user mouseup in canvas area.
          d. After completion, random color will be automatically selected again at the moment that the next time user mousedown     in canvas area again.
*       Rember to check pencil's brush size before you draw.

### 13. Undo
*       User can undo to previous drawing using Undo.
*       If size of canvas area has been changed in the previous drawing, it will restore its previous size after Undo.

### 14. Redo
*       User can redo to next drawing using Redo.
*       If size of canvas area has been changed in the next drawing, it will restore its next size after Redo.

### 15. Clean
*       User can clean current drawing in canvas area using Clean.
*       The storage of undo and redo will also be cleaned after Clean.